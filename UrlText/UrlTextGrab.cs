﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace UrlTextGrab
{
    public class UrlTextGrab:IUrlTextGrab.IUrlTextGrab
    {
        public string Url { get; set; }
        public string Text { get; set; }
        public ICollection<string> Words { get; set; }

        public UrlTextGrab(string url)
        {
            Url = url ?? throw new ApplicationException("Specify the URI of the resource to retrieve.");
            Words = new List<string>();
        }

        public void GetTextFromUrl()
        {
            WebClient webClient = new WebClient();
            using (Stream data = webClient.OpenRead(Url))
            {
                using (StreamReader reader = new StreamReader(data))
                {
                    Text = reader.ReadToEnd();
                }
            }
        }

        public void GetWordsFromUrl()
        {
            GetTextFromUrl();
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(Text);

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//text()[normalize-space(.) != '']"))
            {
                var matches = Regex.Matches(node.InnerText, @"\w+[^\s]*\w+|\w");

                foreach (Match match in matches)
                {
                    var word = match.Value;
                    Words.Add(word);
                }
            }
        }
    }
}
