﻿$(document).ready(function onPageLoad() {
    var animatedProperties = {
        paddingLeft: "20px",
        paddingTop: "20px",
        paddingBottom: "20px",
        paddingRight: "20px",
        opacity: 1
    };

    $(".tag-cloud").animate(animatedProperties, 500);
});