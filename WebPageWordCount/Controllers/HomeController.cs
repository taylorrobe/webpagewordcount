﻿using System;
using System.Net;
using System.Web.Mvc;
using IUrlTextService;
using WebPageWordCount.Models;

namespace WebPageWordCount.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUrlText _urlText;

        public HomeController(IUrlText urlText)
        {
            _urlText = urlText;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new WebPageViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Url, Words")] WebPageViewModel viewModel)
        {
            try
            {
                _urlText.Url = viewModel.Url;
                _urlText.GetTextFromUrl();
                _urlText.GetWordsFromUrl();
                viewModel.Tags = _urlText.GetTags();
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e);
                //user message already handled by validation
            }
            catch (WebException e)
            {
                Console.WriteLine(e);
                ViewBag.ErrorMessage = "Sorry, there is a problem with the URL, please check and try again";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewBag.ErrorMessage = "Sorry, there is a problem";
            }

            return View(viewModel);
        }
    }
}