﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sparc.TagCloud;

namespace WebPageWordCount.Models
{
    public class WebPageViewModel : IEnumerable
    {
        [NotMapped]
        [Required(ErrorMessage = "A URL is required")]
        [DisplayName("URL to get Words from")]
        [StringLength(1024)]
        public string Url { get; set; }

        [NotMapped] public List<string> Words { get; set; }

        [NotMapped] public IEnumerable<TagCloudTag> Tags { get; set; }

        public IEnumerator GetEnumerator()
        {
            return Words.GetEnumerator();
        }
    }
}