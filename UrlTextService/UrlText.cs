﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using IUrlTextService;
using Sparc.TagCloud;
using Unity.Attributes;

namespace UrlTextService
{
    public class UrlText : IUrlText
    {
        public string Url { get; set; }
        public string Text { get; set; }
        public ICollection<string> Words { get; set; }

        public UrlText()
        {

        }

        [InjectionConstructor]
        public UrlText(string url)
        {
            Url = url ?? throw new ApplicationException("Specify the URI of the resource to retrieve.");
            Words = new List<string>();
        }

        public void GetTextFromUrl()
        {
            WebClient webClient = new WebClient();
            try
            {
                using (Stream data = webClient.OpenRead(Url))
                {
                    using (StreamReader reader = new StreamReader(data))
                    {
                        Text = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        public void GetWordsFromUrl()
        {
            Words.Clear();
            //GetTextFromUrl();
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(Text);

            foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//text()[normalize-space(.) != '']"))
            {
                var punctuation = node.InnerText.Where(Char.IsPunctuation).Distinct().ToArray();
                var words = node.InnerText.Split().Select(x => x.Trim(punctuation));
                
                foreach (string word in words)
                {
                    if (word.Length >= 4)
                    {
                        var newWord = word;
                        //remove apostrophe 's' as TagCloudAnalyser splits words on apostrophe
                        if (word.Contains("'s"))
                        {
                            newWord = word.Remove(word.IndexOf("'s", StringComparison.Ordinal), 2);
                        }
                        if (word.Contains("’s"))
                        {
                            newWord = newWord.Remove(word.IndexOf("’s", 2, StringComparison.Ordinal));
                        }

                        //add words that have at least 4 alpha chars (also takes out dates etc)
                        if (Regex.IsMatch(word, @"^\w{4,}$"))
                        {
                            Words.Add(newWord);
                        }
                    }
                }
            }
        }

        public IEnumerable<TagCloudTag> GetTags()
        {
            var settings = new TagCloudSetting();
            settings.MaxCloudSize = 30;
            TagCloudAnalyzer tagCloudAnalyzer = new TagCloudAnalyzer(settings);
            var tags = tagCloudAnalyzer.ComputeTagCloud(Words);
            var tagCloudTags = tags.Shuffle();
            var cloudTags = tagCloudTags.ToList();
            return cloudTags;
        }
    }
}
