﻿using System.Collections.Generic;
using IUrlTextService;
using NUnit.Framework;
using Moq;
using System.Web.Mvc;
using UrlTextService;
using WebPageWordCount.Controllers;

namespace WebPageWordCount.Tests.Controllers
{
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
            var urlTextStub = new Mock<IUrlText>();
            urlTextStub.Object.Url = "https://www.captecsystems.com/";
            urlTextStub.Object.Text = "hello email@whatever.com world";
            urlTextStub.Object.Words = new List<string> { "hello", "world" };
            urlTextStub.SetupAllProperties();
            // Arrange
            HomeController controller = new HomeController(urlTextStub.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
