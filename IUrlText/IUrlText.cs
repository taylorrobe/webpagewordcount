﻿using System.Collections.Generic;

namespace IUrlTextGrab
{
    public interface IUrlTextGrab
    {
        string Url { get; set; }
        string Text { get; set; }
        ICollection<string> Words { get; set; }
        void GetTextFromUrl();
        void GetWordsFromUrl();
    }
}
