﻿using Sparc.TagCloud;
using System.Collections.Generic;

namespace IUrlTextService
{
    public interface IUrlText
    {
        string Url { get; set; }
        string Text { get; set; }
        ICollection<string> Words { get; set; }
        void GetTextFromUrl();
        void GetWordsFromUrl();
        IEnumerable<TagCloudTag> GetTags();
    }
}
