﻿using System.Collections.Generic;
using IUrlTextService;
using NUnit.Framework;
using UrlTextService;

// ReSharper disable once CheckNamespace
namespace UrlTextServiceTests
{
    public class UrlTextServiceTests
    {
        private readonly IUrlText _urlText;

        public UrlTextServiceTests()
        {
            _urlText = new UrlText("https://www.captecsystems.com/");
        }

        [Test]
        public void GetWordsFromUrlTest()
        {
            var expectedResult = new List<string> {"hello", "world"};
            _urlText.Text = "hello email@whatever.com world";
            _urlText.GetWordsFromUrl();
            CollectionAssert.AreEquivalent(expectedResult, _urlText.Words);
        }

        [Test]
        public void GetWordsFromUrlTestSmallWords()
        {
            var expectedResult = new List<string> { "hello", "world" };
            _urlText.Text = "hello me it any 16/12/2018 world";
            _urlText.GetWordsFromUrl();
            CollectionAssert.AreEquivalent(expectedResult, _urlText.Words);
        }
    }
}